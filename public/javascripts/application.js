// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults


jQuery(document).ready(function() {


    jQuery(".text-input").each(function() {
        if (jQuery(this).val() == "") {
            jQuery(this).val(jQuery(this).attr("default"));
        }
    });

    jQuery(".text-input").focus(function() {
        if (jQuery(this).val() == jQuery(this).attr("default")) {
            jQuery(this).val("");
        }
    });

    jQuery(".text-input").blur(function() {
        if (jQuery(this).val() == "") {
            jQuery(this).val(jQuery(this).attr("default"));
        }
    });

    jQuery(".password-input").each(function() {
        if (jQuery(this).val() == "") {
            this.type="text";
            jQuery(this).val(jQuery(this).attr("default"));
        }
    });

    //TODO: NEED TO VERIFY THAT THIS IS OK IN IE7+
    jQuery(".password-input").focus(function() {
        if (jQuery(this).val() == jQuery(this).attr("default")) {
            this.type="password";
            jQuery(this).val("");
        }
    });

    jQuery(".password-input").blur(function() {
        if (jQuery(this).val() == "") {
            this.type="text";
            jQuery(this).val(jQuery(this).attr("default"));
        }
    });

});



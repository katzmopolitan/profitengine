function requestToChangeStatus(element) {
    link = $(element).attr("href");
    id = $(element).parent().attr("id");
    $(element)
            .parents(".amazon_listing_reprice")
            .replaceWith('<img id="' + id + '" src="images/spinner.gif" />');

    $.ajax({
        type: "GET",
        url: link,
        dataType: 'script'
    });

}

function repriceEnable(element) {
    id = $(element).parent().attr("id");
    listing_id = $(element).attr("listing_id");
    price = $(element).siblings(".low_limit_price").val()
    link = "/amazon_listings/" + listing_id + "/reprice/enable";

    $(element)
            .parents(".amazon_listing_reprice")
            .replaceWith('<img id="' + id + '" src="images/spinner.gif" />');

    $.ajax({
        type: "GET",
        url: link,
        data: {
            "low_price_limit" : price
        },
        dataType: 'script'
    });

}

$(document).ready(function() {
    $(".disabled-checkbox").tipTip();
    $("#reprice_button").tipTip();
    $(".tooltip").tipTip();

    $("#update_image").bind("click", function() {
        $.ajax({
            type: "GET",
            url: $(this).attr("data-refresh-url"),
            success: function() {
                $(".inventory_status .status_indicator").removeClass("never");
                $(".inventory_status .status_indicator").text("pending");
                $("#update_image").hide();
            }
        });
    });

    $(".amazon_listing_checkbox").live("click", function() {
        requestToChangeStatus($(this));
    });

    $("#reprice_all").live("click", function() {
        $(".amazon_listing_checkbox").filter(":not(:checked)").each(function() {
            repriceEnable($(this));
        });
    });

    $('.low_limit_price').change(function() {
        $(this).addClass("updated_low_price")
    });

    $('.low_limit_price').live("keyup", function() {
        $(this).addClass("updated_low_price")
    });

    $("#update_selected").click(function() {
        $(".updated_low_price").each(function() {
            repriceEnable($(this).siblings(".amazon_listing_checkbox"));
        });
    });

    $('.low_limit_price').live('keypress', function(e) {
        if (e.keyCode == 13) {
            repriceEnable($(this).siblings(".amazon_listing_checkbox"));
        }
    });
});

var uvOptions = {};
(function() {
    var uv = document.createElement('script');
    uv.type = 'text/javascript';
    uv.async = true;
    uv.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'widget.uservoice.com/poQq2H8wmqQmXxfH4ZPqA.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(uv, s);
})();
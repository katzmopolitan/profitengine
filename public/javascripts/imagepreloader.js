function preloadImages(imgs){	
	var picArr = [];	
	for (i = 0; i<imgs.length; i++){			
		picArr[i]= new Image(100,100); 
		picArr[i].src=imgs[i]; 			
	}	
}	
preloadImages([
	'../images/thumb-hover.png',
	'../images/number-hover.png',
	'../images/more-hover.png',
	'../images/button-green.png',
	'../images/more.png',
	'../images/more.png',
	'../images/submenu.png',
	'../images/menu-hover.png']);
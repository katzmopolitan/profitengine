﻿function defaultText(field, fname, reset) {
	if(field.value==fname) {
		field.value='';
	}
	else {
		if(!reset) return;
		if(field.value=='') {
			field.value=fname;
		}
	}
}
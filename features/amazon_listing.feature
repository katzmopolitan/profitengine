Feature: listings


  Scenario: User dashboard
    Given I am logged in
    Then I should see "Signed in successfully."
    And I should see "Account Settings"

  Scenario: Successfully add amazon account
    Given Amazon API is mocked out to return success
    When I am logged in
    And I follow "Add Amazon Account"
    Then the current page should be secure
    And I should see "Account Settings"
    And I fill in "amazon_account_merchant_id" with "1234567"
    And I fill in "amazon_account_marketplace_id" with "987654"
    And I press "Submit"
    And I should see "Your amazon account is connected "
    And an amazon account should exist with merchant_id: "1234567", marketplace_id: "987654"

  Scenario: Update previously added amazon account
    Given Amazon API is mocked out to return success
    And I am logged in
    And I have an amazon account connected
    Then the current page should be secure
    When I follow "Account"
    And I follow "amazon account"
    And I press "Submit"
    Then I should see "Changes saved successfully"

  Scenario: Unsuccessful update of the amazon account
    Given Amazon API is mocked out to return success
    And I am logged in
    And I have an amazon account connected
    And Amazon API is mocked out to return an error
    When I follow "Account"
    And I follow "amazon account"
    And I fill in "amazon_account_merchant_id" with "asdfasdf"
    And I press "Submit"
    Then I should see "Could not update account"

  Scenario: Amazon account could not be connected
    Given Amazon API is mocked out to return an error
    When I am logged in
    And I follow "Add Amazon Account"
    And I should see "Account Settings"
    And I fill in "amazon_account_merchant_id" with "1234567"
    And I fill in "amazon_account_marketplace_id" with "987654"
    And I press "Submit"
    And an amazon account should not exist with merchant_id: "1234567", marketplace_id: "987654"

  Scenario: Incorrect format for account
    When I am logged in
    And I follow "Add Amazon Account"
    And I should see "Account Settings"
    And I fill in "amazon_account_merchant_id" with "1234 567"
    And I fill in "amazon_account_marketplace_id" with "987 654"
    And I press "Submit"
    And an amazon account should not exist with merchant_id: "1234567", marketplace_id: "987654"

  Scenario: Show my inventory if I have it
    Given Amazon API is mocked out to return success
    And I am logged in
    And I have an amazon account connected
    And I have inventory
    And I follow "Inventory"
    Then I should see "ASIN"
    When I follow "2"
    Then I should see "ASIN"

  Scenario: I enable reprice for my amazon account
    Given Amazon API is mocked out to return success
    And I am logged in
    And I have an amazon account connected
    And I have inventory
    And I follow "Inventory"
    And I toggle preprice button
    Then I should see amazon listings items table
    And an amazon account should exist with reprice: true

  Scenario: I disable reprice for my amazon account
    Given Amazon API is mocked out to return success
    And I am logged in
    And I have an amazon account connected
    And I have enabled reprice for my amazon account
    And I have inventory
    And I follow "Inventory"
    And I toggle preprice button
    Then I should see amazon listings items table
    And an amazon account should exist with reprice: false

  Scenario:  I enable/disable reprice for an individual item
    Given Amazon API is mocked out to return success
    And I am logged in
    And I have an amazon account connected
    And I have enabled reprice for my amazon account
    And I have inventory of one product
    And all amazon listing are disabled from reprice
    When I enable reprice for my last item
    Then all amazon listing are enabled for reprice
    And I follow "Inventory"
    And the "amazon_listing_checkbox_0" checkbox should be checked
    When I disable reprice for my last item
    And I follow "Inventory"
    And the "amazon_listing_checkbox_0" checkbox should not be checked
    And all amazon listing are disabled from reprice


  Scenario:  Attempt to enable/disable reprice for an individual item if amazon account is not enabled for reprice
    Given Amazon API is mocked out to return success
    And I am logged in
    And I have an amazon account connected
    And I have disabled reprice for my amazon account
    And I have inventory of one product
    And all amazon listing are disabled from reprice
    And I enable reprice for my last item
    Then I should see "Amazon Account is not configured for repricing"
    And all amazon listing are disabled from reprice
    And I disable reprice for my last item
    Then I should see "Amazon Account is not configured for repricing"
    And all amazon listing are disabled from reprice

  Scenario: Attempt to make changes if amazon account no longer works
    Given Amazon API is mocked out to return success
    And I am logged in
    And I have an amazon account connected
    And I have disabled reprice for my amazon account
    And I have inventory
    And Amazon API is mocked out to return an error
    When I follow "Inventory"
    And I toggle preprice button
    Then I should see "There was an error with your account"
    And an amazon account should exist with reprice: false

  Scenario: Give a call to action from inventory page if account isn't set up
    And I am logged in
    When I follow "Inventory"
    Then the current page should be secure
    And I follow "connect your account"
    Then I should be on new amazon account page

  Scenario: If an account was never repriced, show it
    Given Amazon API is mocked out to return success
    And I am logged in
    And I have an amazon account connected
    When I follow "Inventory"
    Then I should see "never"

  Scenario: If an account was repriced recently show it
    Given Amazon API is mocked out to return success
    And I am logged in
    And I have an amazon account connected
    And my account was repriced on "Oct 12, 2011"
    When I follow "Inventory"
    Then I should see "October 12, 2011"

  @javascript
  Scenario: Update global reprice option from listings page
    Given Amazon API is mocked out to return success
    And I am logged in
    And I wait 1 second
    And I have an amazon account connected
    And I have disabled reprice for my amazon account
    Then an amazon account should exist with reprice: false
    When I follow "Inventory"
    And I follow "reprice-button"
    And I wait 1 second
    Then an amazon account should exist with reprice: true
    And I follow "reprice-button"
    And I wait 1 second
    Then an amazon account should exist with reprice: false

  @javascript
  Scenario: Enable/disable reprice of a single item
    Given Amazon API is mocked out to return success
    And I am logged in
    And I wait 1 second
    And I have an amazon account connected
    And I have inventory of one product
    And I have enabled reprice for my amazon account
    When I follow "Inventory"
    Then the "amazon_listing_enable" checkbox should not be checked
    And I click "listing reprice checkbox"
    And I wait 1 second
    #TODO: need to see why we cannot test on the same page
    When I follow "Inventory"
    Then the "amazon_listing_enable" checkbox should be checked
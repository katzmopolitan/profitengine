def email_confirmed_user_me(opts={})
  @email_confirmed_user_me ||= create_model("an email_confirmed_user \"me\"", opts)
  @email_confirmed_user_me.reload
end

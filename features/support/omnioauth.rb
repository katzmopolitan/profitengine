CUCUMBER_FACEBOOK_ID="1234556"
CUCUMBER_FACEBOOK_EMAIL="cucumber@rentini.com"
OmniAuth.config.add_mock(:facebook,
                         {
                             :extra => {
                                 :user_hash=>{
                                     "email"=>CUCUMBER_FACEBOOK_EMAIL,
                                     "first_name"=>"cucumber",
                                     "last_name"=>"facebook",
                                     "verified"=>true,
                                     "id"=>CUCUMBER_FACEBOOK_ID
                                 }
                             }
                         }
)

OmniAuth.config.add_mock(:google_apps,
                         {
                             :user_info => {
                                     "email"=>"cucumber@rentini.com",
                                     "first_name"=>"cucumber",
                                     "last_name"=>"gmail"
                             }
                         }
)

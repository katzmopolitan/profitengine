And /I leave unchecked "(.*)"/ do |ignore|
  #empty
end

When(/^I go back$/) do
  #visit page.driver.last_request.env['HTTP_REFERER']
  visit request.env["HTTP_REFERER"]
end

Then /^I should not see an error$/ do
  (200 .. 399).should include(page.status_code)
end

Then /^I should see an error$/ do
  (400 .. 599).should include(page.status_code)
end

Then /^I should be redirected to "([^"]*)"$/ do |url|
  requested_url = page.driver.request.env['rack.url_scheme'] + '://' + page.driver.request.env['SERVER_NAME']
  requested_url.should == url
end

When /^the current page should be (.*) protocol$/ do |protocol|
  (current_url =~ /^#{protocol}:\/\//).should_not be_nil
end

Then /^current page should be secure$/ do
  (current_url =~ /^https:\/\//).should_not be_nil
end

Given /^I visit "([^"]*)"$/ do |url|
  visit(url)
end

module CucumberRailsDebug
  def where
    puts current_url
  end

  def html
    puts page.body.gsub("\n", "\n ")
  end

  def display(decoration="\n#{'*' * 80}\n\n")
    puts decoration
    yield
    puts decoration
  end
end

World(CucumberRailsDebug)

Then "what" do
  display do
    where
    html
    where
  end
end
#Note that you need to tag the scenario with @allow-rescue to test that an error is shown like this
#@allow-rescue
#Scenario: Accessing the admin area requires a login
#  When I go to the admin area
#  Then I should see an error

Then /^the current page should be secure$/ do
  (current_url =~ /^https:\/\//).should_not be_nil
end


When /^I wait (\d+) second(|s)$/ do |arg1, ignore|
  sleep(arg1.to_i)
end


When /^(?:|I )click "([^"]*)"$/ do |locator|
  msg = "No element with selector '#{locator}' found, make sure value is present in selectors.rb"
  wait_until do
    selector_for(locator)
  end
  selector = selector_for(locator)
  find(:css, selector, :message => msg).click
end
Given /^I am logged in$/ do
  And %{I am a new, authenticated user}
end

Given /^I am logged in with email "([^"]*)"$/ do |email|
  email_confirmed_user_me(:email=>email)
  And %{I am a new, authenticated user}
end

Given /^an authenticated user exists with email "([^"]*)"$/ do |email|
  create_model("an email_confirmed_user \"me\"", :email=>email)
  And %{all emails have been delivered}
end

Given /^I am a new, authenticated user$/ do
  user = email_confirmed_user_me
  And %{I go to the sign in page}
  And %{I fill in "user_email" with "#{user.email}" within ".user_new"}
  And %{I fill in "user_password" with "#{user.password || 'secret123'}" within ".user_new"}
  And %{I press "Login"}
end

And /^they setup their password$/ do
  And %{I fill in "Password" with "secret"}
  And %{I press "Update"}
end

Given /^user "([^"]*)" is registered$/ do |email|
  Factory(:email_confirmed_user, :email=>email)
end

Given /^I am an unregistered with with email "([^"]*)"$/ do |email|
  @unregistered_user_email = email
end

And /^I should see my email$/ do
   And %{I should see "#{email_confirmed_user_me.email}"}
end

And /^I log out$/ do
  visit("/users/sign_out")
  @email_confirmed_user_me = nil
end


Given /^I am registered with facebook$/ do
  Authentication.create(:provider=>"facebook",
                        :provider_id=>CUCUMBER_FACEBOOK_ID,
                        :user=>Factory(:user, :email=>CUCUMBER_FACEBOOK_EMAIL)
  )
end

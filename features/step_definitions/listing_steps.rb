Given /^my account was repriced on "([^"]*)"/ do |time|
  @email_confirmed_user_me.amazon_account.update_attribute("reprice_last_run", Time.parse(time))
end

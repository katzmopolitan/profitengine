Given /^Amazon API is mocked out to return an error$/ do

  module AmazonMWS
    class Base
      def get_report_request_count

        response = {}

        def response.accessors
          ["code"]
        end

        def response.code
          "AccessDenied"
        end

        response

      end
    end
  end

end

Given /^Amazon API is mocked out to return success$/ do

  module AmazonMWS
    class Base
      def get_report_request_count

        response = {}

        def response.accessors
          ["nothing"]
        end

        def response.code
          "Success"
        end

        response

      end
    end
  end
end

When /^I have an amazon account connected$/ do
  a = Factory.build(:amazon_account, :user=>@email_confirmed_user_me)
  a.save!
end

Given /^I have inventory$/ do
  generate_amazon_listings_for_user(@email_confirmed_user_me)
end

Given /^I have inventory of one product$/ do
  generate_amazon_listings_for_user(@email_confirmed_user_me, 1)
end

When /^I toggle preprice button$/ do
  And %{I follow "reprice-button"}
end

Then /^I should see amazon listings items table$/ do
  And %{I should see "" within "#listing_items"}
end

Given /^I have enabled reprice for my amazon account$/ do
  @email_confirmed_user_me.amazon_account.reprice=true
  @email_confirmed_user_me.amazon_account.save!
end

Given /^I have disabled reprice for my amazon account$/ do
  @email_confirmed_user_me.amazon_account.reprice=false
  @email_confirmed_user_me.amazon_account.save!
end

Then /^all amazon listing are disabled from reprice$/ do
  AmazonListing.all.each do |listing|
    listing.reprice_enabled?.should be_false
  end
end

Then /^all amazon listing are enabled for reprice$/ do
  AmazonListing.all.each do |listing|
    listing.reprice_enabled?.should be_true
  end
end

Given /^I enable reprice for my last item$/ do
  visit amazon_listing_reprice_enable_url(:amazon_listing_id => @email_confirmed_user_me.amazon_listings.last,
                                          :secure=>true)
end

Given /^I disable reprice for my last item$/ do
  visit amazon_listing_reprice_disable_url(:amazon_listing_id => @email_confirmed_user_me.amazon_listings.last,
                                           :secure=>true)
end


include ActionView::Helpers::NumberHelper

def generate_amazon_listings_for_user(user, count = 12)

  listing_id = 0

  count.times do
    create_amazon_listing(listing_id, user)
    listing_id = listing_id + 1
  end

end


def create_amazon_listing(listing_id, user)
  Factory(:amazon_listing,
          :amazon_account_id=>user.amazon_account.id,
          :item_name => "Item #{listing_id}",
          :item_description => "Item #{listing_id} description hey hey hey hally hally",
          :listing_id=> listing_id,
          :sku=>"1023",
          :price=>number_with_precision(rand*10, :precision =>2).to_f,
          :lowest_used_price=>number_with_precision(rand*10, :precision =>2).to_f,
          :lowest_new_price=>number_with_precision(rand*10, :precision =>2).to_f,
          :lowest_collectable_price=>0,
          :lowest_refurbished_price=>0,
          :quantity=>1,
          :open_date=>"2009-10-18 21:31:27",
          :image_url=>nil,
          :item_is_marketplace=>"Y",
          :product_id_type=>3,
          :zshop_shipping_fee=>0,
          :item_note=>"ORIGINAL ITEM WITH ORIGINAL ARTWORK AND CASE! PLEASE NOTE, THERE IS A SIGNATURE ON THE COVER, BUT NOT SURE IF IT'S REALLY BY THE ARTIST. WILL REFUND IF THERE ARE ISSUES! ALSO WILL REFUND -$1.00 ON SHIPPING FOR ANY ADDITIONAL CD/DVD/GAME/BOOK ORDER THAT'S MAILED TOGETHER!",
          :item_condition=>2,
          :zshop_category1=>0,
          :zshop_browse_path=>nil,
          :zshop_storefront_feature=>nil,
          :asin1=>"B00067WFFA",
          :asin2=>nil,
          :asin3=>nil,
          :will_ship_internationally=>1,
          :expedited_shipping=>"N",
          :zshop_boldface=>nil,
          :product_id=>(rand*1000000000).to_i,
          :bid_for_featured_placement=>nil,
          :add_delete=>nil,
          :pending_quantity=>"DEFAULT",
          :fulfillment_channel=>"MERCHANT",
          :created_at=>"2011-05-01 14:19:04",
          :updated_at=>"2011-05-01 14:19:14"
  )
end
Feature: Signing up

  Scenario: Creating a new account
    When I go to the signup page
    Then the current page should be secure
    And I fill in "user[email]" with "ilya@gmail.com" within ".user_new"
    And I fill in "user[password]" with "secretpass" within ".user_new"
    And I fill in "user[password_confirmation]" with "secretpass" within ".user_new"
    And I press "Submit"
    And a user "l" should exist with email: "ilya@gmail.com"
    Then I should see "Welcome! You have signed up successfully"
    Then I should be on the amazon_accounts page
    
  Scenario: Sign up with facebook for the first time
    Given I go to the signin page
    Then the current page should be secure
    And a user should not exist with email: "cucumber@rentini.com"
    And I follow "Facebook Connect"
    And I should see "Successfully authorized from Facebook account"
    And a user should exist with email: "cucumber@rentini.com"
    Then I should be on the amazon_accounts page
    
  Scenario:  Sign in with google
    When I go to the signin page
    And I follow "Google Connect"
    Then the current page should be secure
    And I should see "Successfully authorized from Gmail account."
    And a user should exist with email: "cucumber@rentini.com"

  Scenario: Signin with facebook for the second time
    Given I am registered with facebook
    Then a user should exist with email: "cucumber@rentini.com"
    When I go to the signin page
    And I follow "Facebook Connect"
    And I should see "Successfully authorized from Facebook account"
    And 1 users should exist
    And a user should exist with email: "cucumber@rentini.com"

  Scenario: Reset your password
    Given a user exists with email: "ilya@profit-engine.com", password: "secret"
    And all emails have been delivered
    When I go to the sign in page
    And I follow "Forgot your password?"
    And I fill in "user_email" with "ilya@profit-engine.com"
    And I press "Submit" within ".contact_form"
    And 1 email should be delivered to "ilya@profit-engine.com"

  Scenario: If I already have inventory, after sign in I should be on inventory page
    Given Amazon API is mocked out to return success
    And I am logged in with email "ilya@gmail.com"
    And I have an amazon account connected
    And I have inventory
    And I log out
    When I go to the sign in page
    And I fill in "user[email]" with "ilya@gmail.com"
    And I fill in "user[password]" with "secret123"
    And I press "Login"
    Then I should be on the amazon_listings page

  Scenario: If I do not have inventory, after sign in I should be on inventory page
    Given Amazon API is mocked out to return success
    And I am logged in with email "ilya@gmail.com"
    And I log out
    When I go to the sign in page
    And I fill in "user[email]" with "ilya@gmail.com"
    And I fill in "user[password]" with "secret123"
    And I press "Login"
    Then I should be on the amazon_accounts page
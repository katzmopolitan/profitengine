Feature: In order to get customer service
  User want to contact us with questions


  Scenario: Registered user send a question
    Given I am logged in
    When I go to the contact page
    And I fill in "Name" with "Elmo"
    And I fill in "Email" with "elmo@sesame.com"
    And I fill in "Message" with "How do I make money?"
    And I press "Send"
    Then I should see "Your message has been sent"
    Then 1 emails should be delivered
    And email should be delivered to "cucumber@PROFIT-ENGINE.COM"

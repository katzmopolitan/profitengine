Feature: social advertizing

  Scenario: By default social advertizing is enabled
    Given Amazon API is mocked out to return success
    And I am logged in
    And I have an amazon account connected
    And I have inventory
    And I follow "Inventory"
    And I toggle preprice button
    Then I should see "Advertize"

  Scenario: allow users to disable social advertizing
    Given Amazon API is mocked out to return success
    And I am logged in
    And I have an amazon account connected
    And I have inventory
    And I follow "Inventory"
    And I toggle preprice button
    And I follow "x"
    And I follow "Inventory"
    Then I should not see "Advertize"

  Scenario: change settings of social ads
    Given Amazon API is mocked out to return success
    And I am logged in
    And I have an amazon account connected
    And I have inventory
    Then an amazon account should exist
    And I follow "Account"
    And I follow "amazon account"
    And I uncheck "Social Advertizing"
    And I press "Submit"
    And I follow "Inventory"
    Then I should not see "Advertize"
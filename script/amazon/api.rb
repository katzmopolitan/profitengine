unless Rails.env.production?
  puts "cannot run this in dev"
  exit
end


require "hmac-sha1.rb"

class AmazonMWS::Authentication

  class Signature < String #:nodoc:

    def string_to_sign(verb, querystring)
      verb   = verb.to_s.upcase
      string = "#{verb}\n#{AmazonMWS::DEFAULT_HOST}\n/Products/\n#{querystring}"
    end

  end

end


class AmazonMWS::Authentication

  class QueryString < String
    def initialize(params = {})
      query_params = {
              'AWSAccessKeyId'   => params[:access_key],
              'Marketplace'      => params[:marketplace_id],
              'Merchant'         => params[:merchant_id],
              'SellerId'         => params[:merchant_id],
              'SignatureMethod'  => Signature::METHOD,
              'SignatureVersion' => Signature::VERSION,
              'Timestamp'        => Time.now.iso8601,
              'Version'          => params[:version] || AmazonMWS::Authentication::VERSION
      }


      # Add any params that are passed in via uri before calculating the signature
      query_params = query_params.merge(params[:query_params] || {})
      # Calculate the signature
      query_params['Signature'] = Signature.new(query_params, params)

      puts query_params.inspect
      self << formatted_querystring(query_params)
    end
  end

end


module AmazonMWS

  class GetStatusResponse < Response

    xml_name "GetServiceStatus"
    result = "GetServiceStatusResult"

    xml_reader :status, :in => result
    xml_reader :timestamp, :in => result
    xml_reader :request_id, :in => "ResponseMetadata"
  end

end

module AmazonMWS

  module Report

    def get_service_status(params = {})
      response = get("/Products/", {"Action" => "GetServiceStatus"})
      GetStatusResponse.format(response)
    end

  end
end


user = User.find_by_email("juliankh@optonline.net") if Rails.env.production?
account = user.amazon_account
merchant_id = account.merchant_id
marketplace_id = account.marketplace_id
mws = AmazonMWS::Base.new(
        "access_key"=>::AWS_ACCESS_KEY,
        "secret_access_key"=>::AWS_SECRET_KEY,
        "merchant_id"=>merchant_id,
        "marketplace_id"=>marketplace_id,
        "version"=>"2011-10-11"
)

r=mws.get_service_status




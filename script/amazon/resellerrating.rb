require 'nokogiri'
require 'open-uri'
require 'set'
require 'csv'



homepage_url="https://www.resellerratings.com/merchant-members"
homepage = Nokogiri::HTML(open(homepage_url))
store_links = homepage.css(".tableRatingsSideBar a")
store_urls = Set.new

store_links.each do |link|
  relative_url=link.attributes["href"].value
  absolute_url="https://www.resellerratings.com#{relative_url}"
  store_urls<<absolute_url
end

store_urls.each do |url|
  puts url
end       ;1

stores = {}
store_urls.each do |url|
  puts "doing #{url}"
  doc = Nokogiri::HTML(open(url))
  store = doc.css(".fn").first.content
  stores[store]={}
  stores[store][:link]=url

  site_url = doc.css(".hreview-aggregate a")[1].content
  stores[store][:store_url]=site_url


  doc.css('#tab2 .inset li').each do |contact|
    info = contact.children[0].content
    case info
      when /Address/
        stores[store][:address]=  contact.children[1].content.gsub(/^\s/,'')
      when /Contact/
        stores[store][:email]=  contact.children[1].content.gsub(/^\s/,'')
      when /Phone/
        stores[store][:phone]=  contact.children[1].content.gsub(/^\s/,'')
      when /Customer Support/
        stores[store][:phone1]=  contact.children[1].content.gsub(/^\s/,'')
      when /Fax/
        stores[store][:fax]=  contact.children[1].content.gsub(/^\s/,'')
    end
  end
end


output_path = "/Users/ilyakatz/amazon_resellers.csv"
CSV.open(output_path, "wb") do |csv|
  csv << ["store", "url","reseller_url","address","email","phone","phone1","fax"]
  stores.each do |k, v|
    csv << [k,v[:store_url],v[:link],v[:address],v[:email],v[:phone],v[:phone1],v[:fax]]
  end
end;1
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
#   Mayor.create(:name => 'Daley', :city => cities.first)

include ActionView::Helpers::NumberHelper

class AmazonAccount < ActiveRecord::Base
  def validate_amazon_account_credentials
  end
end


def generate_users
  @users = ['ilyakatz@gmail.com', 'juliankh@optonline.net', 'ivo@piskov.net']
  puts "seeding users"

  @users.each do |email|
    user = User.find_by_email(email)
    if user.nil?
      User.create(:email=>email, :password=>'secret123', :confirmed_at=>Time.now)
      #user.confirm!
    end
  end

end

#link to product for this merchant is
#http://www.amazon.com/dp/B001CSMJKI?m=A1NVJAK8CL82FZ
#http://www.amazon.com/dp/B0007V9R2O?m=A38AZVNJ7R3BL1
def create_amazon_listing(listing_id, user)
  AmazonListing.create(
      :amazon_account_id=>user.amazon_account.id,
      :item_name => "Item #{listing_id}",
      :item_description => "Item #{listing_id} description hey hey hey hally hally",
      :listing_id=> listing_id + 100,
      :sku=>rand(36**9).to_s(36)[0..7],
      :price=>number_with_precision(rand(1000000)/100.00, :precision =>2).to_f,
      :lowest_used_price=>number_with_precision(rand*10, :precision =>2).to_f,
      :lowest_new_price=>number_with_precision(rand*10, :precision =>2).to_f,
      :lowest_collectable_price=>0,
      :lowest_refurbished_price=>0,
      :quantity=>rand(2000),
      :open_date=>"2009-10-18 21:31:27",
      :image_url=>nil,
      :item_is_marketplace=>"Y",
      :product_id_type=>3,
      :zshop_shipping_fee=>0,
      :item_note=>"ORIGINAL ITEM WITH ORIGINAL ARTWORK AND CASE! PLEASE NOTE, THERE IS A SIGNATURE ON THE COVER, BUT NOT SURE IF IT'S REALLY BY THE ARTIST. WILL REFUND IF THERE ARE ISSUES! ALSO WILL REFUND -$1.00 ON SHIPPING FOR ANY ADDITIONAL CD/DVD/GAME/BOOK ORDER THAT'S MAILED TOGETHER!",
      :item_condition=>rand(11) + 1,
      :zshop_category1=>0,
      :zshop_browse_path=>nil,
      :zshop_storefront_feature=>nil,
      :asin1=>rand(36**9).to_s(36)[0..9],
      :asin2=>nil,
      :asin3=>nil,
      :will_ship_internationally=>1,
      :expedited_shipping=>"N",
      :zshop_boldface=>nil,
      :product_id=>(rand*1000000000).to_i,
      :bid_for_featured_placement=>nil,
      :add_delete=>nil,
      :pending_quantity=>"DEFAULT",
      :fulfillment_channel=>"MERCHANT",
      :created_at=>"2011-05-01 14:19:04",
      :updated_at=>"2011-05-01 14:19:14"
  )
end

def generate_amazon_listings

  listing_id = 0
  if AmazonListing.count == 0
    User.all.each do |user|
      unless user.amazon_account.nil?
        listings = YAML::load(File.open("#{Rails.root}/db/fixtures/amazon_listings.yml"))
        listings.each do |listing|
          l = listing.clone
          l.amazon_account=user.amazon_account
          l.save!
        end
      else
        puts "no amazon account for #{user.email}"
      end
    end
  end

end

def generate_controller_threads
  puts "seeding controller thread settings"
  ControllerThread.create(:thread_type=>'CENTRAL', :active=>true, :sleep_secs=>20)
  ControllerThread.create(:thread_type=>'RE_PRICE', :active=>true, :sleep_secs=>25)
  ControllerThread.create(:thread_type=>'GET_INVENTORY', :active=>true, :sleep_secs=>30)
end

def generate_amazon_accounts
  User.all.each do |user|
    puts "seeding amazon accounts for user #{user.email}"
    amazon_account = AmazonAccount.new
    amazon_account.sleep_secs = 300
    if (user.email.eql?('juliankh@optonline.net'))
      amazon_account.marketplace_id = 'ATVPDKIKX0DER'
      amazon_account.merchant_id = 'A25MQYYQMT88F2'
      amazon_account.reprice = true
      amazon_account.get_inventory = true
    else
      amazon_account.marketplace_id = (rand*100000000).to_i
      amazon_account.merchant_id = (rand*100000000).to_i
      amazon_account.reprice = false
    end
    amazon_account.save!
    user.amazon_account = amazon_account
  end
end

def generate_amazon_listings_custom_infos
  puts "seeding amazon accounts custom infos for ilyakatz@gmail.com"
  AmazonListingsCustomInfo.create(:amazon_account_id=>AmazonAccount.where(:user_id=>User.where(:email=>"ilyakatz@gmail.com").first.id).first.id, :listing_id=>"0823I7CK3MX", :sku=>5692, :reprice=>true, :low_price_limit=>0.50)
  puts "seeding amazon accounts custom infos for juliankh@optonline.net"

  AmazonListingsCustomInfo.create(:amazon_account_id=>AmazonAccount.where(:user_id=>User.where(:email=>"juliankh@optonline.net").first.id).first.id, :listing_id=>"0920IXRYQOF", :sku=>5787, :reprice=>true, :low_price_limit=>0.99)
  AmazonListingsCustomInfo.create(:amazon_account_id=>AmazonAccount.where(:user_id=>User.where(:email=>"juliankh@optonline.net").first.id).first.id, :listing_id=>"0203I3C3SW3", :sku=>5681, :reprice=>true, :low_price_limit=>0.99)
end

def export_data
  File.open('/tmp/data.txt', 'w') do |f2|
    f2.puts(YAML::dump(AmazonListing.limit(100)))
  end
end

generate_users
generate_amazon_accounts
generate_amazon_listings
generate_amazon_listings_custom_infos
generate_controller_threads
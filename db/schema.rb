# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121108041658) do

  create_table "amazon_accounts", :force => true do |t|
    t.integer  "user_id"
    t.string   "merchant_id"
    t.string   "marketplace_id"
    t.boolean  "reprice"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "social_ads",             :default => true
    t.integer  "sleep_secs"
    t.datetime "reprice_last_run"
    t.boolean  "get_inventory"
    t.datetime "get_inventory_last_run"
  end

  create_table "amazon_listings", :force => true do |t|
    t.integer  "amazon_account_id"
    t.string   "item_name"
    t.text     "item_description"
    t.string   "listing_id"
    t.string   "sku"
    t.float    "price"
    t.float    "lowest_used_price"
    t.float    "lowest_new_price"
    t.float    "lowest_collectable_price"
    t.float    "lowest_refurbished_price"
    t.integer  "quantity"
    t.datetime "open_date"
    t.text     "image_url"
    t.string   "item_is_marketplace"
    t.integer  "product_id_type"
    t.float    "zshop_shipping_fee"
    t.text     "item_note"
    t.integer  "item_condition"
    t.integer  "zshop_category1"
    t.string   "zshop_browse_path"
    t.string   "zshop_storefront_feature"
    t.string   "asin1"
    t.string   "asin2"
    t.string   "asin3"
    t.string   "will_ship_internationally"
    t.string   "expedited_shipping"
    t.string   "zshop_boldface"
    t.string   "product_id"
    t.string   "bid_for_featured_placement"
    t.string   "add_delete"
    t.integer  "pending_quantity"
    t.string   "fulfillment_channel"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "amazon_listings", ["amazon_account_id"], :name => "index_amazon_listings_on_amazon_account_id"
  add_index "amazon_listings", ["listing_id"], :name => "index_amazon_listings_on_listing_id"
  add_index "amazon_listings", ["sku"], :name => "index_amazon_listings_on_sku"

  create_table "amazon_listings_backups", :force => true do |t|
    t.integer  "amazon_account_id"
    t.string   "item_name"
    t.text     "item_description"
    t.string   "listing_id"
    t.string   "sku"
    t.float    "price"
    t.float    "lowest_used_price"
    t.float    "lowest_new_price"
    t.float    "lowest_collectable_price"
    t.float    "lowest_refurbished_price"
    t.integer  "quantity"
    t.datetime "open_date"
    t.text     "image_url"
    t.string   "item_is_marketplace"
    t.integer  "product_id_type"
    t.float    "zshop_shipping_fee"
    t.text     "item_note"
    t.text     "item_condition"
    t.integer  "zshop_category1"
    t.string   "zshop_browse_path"
    t.string   "zshop_storefront_feature"
    t.string   "asin1"
    t.string   "asin2"
    t.string   "asin3"
    t.string   "will_ship_internationally"
    t.string   "expedited_shipping"
    t.string   "zshop_boldface"
    t.string   "product_id"
    t.string   "bid_for_featured_placement"
    t.string   "add_delete"
    t.integer  "pending_quantity"
    t.string   "fulfillment_channel"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "amazon_listings_custom_infos", :force => true do |t|
    t.integer  "amazon_account_id"
    t.string   "listing_id"
    t.string   "sku"
    t.boolean  "reprice"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "low_price_limit"
  end

  add_index "amazon_listings_custom_infos", ["amazon_account_id"], :name => "index_amazon_listings_custom_infos_on_amazon_account_id"
  add_index "amazon_listings_custom_infos", ["listing_id"], :name => "index_amazon_listings_custom_infos_on_listing_id"
  add_index "amazon_listings_custom_infos", ["sku"], :name => "index_amazon_listings_custom_infos_on_sku"

  create_table "amazon_unshipped_items", :force => true do |t|
    t.integer  "amazon_account_id"
    t.string   "order_id"
    t.string   "order_item_id"
    t.string   "status"
    t.datetime "purchase_date"
    t.datetime "payments_date"
    t.datetime "reporting_date"
    t.datetime "promise_date"
    t.integer  "days_past_promise"
    t.string   "buyer_email"
    t.string   "buyer_name"
    t.string   "buyer_phone_number"
    t.string   "sku"
    t.string   "product_name"
    t.integer  "quantity_purchased"
    t.integer  "quantity_shipped"
    t.integer  "quantity_to_ship"
    t.string   "ship_service_level"
    t.string   "recipient_name"
    t.string   "ship_address_1"
    t.string   "ship_address_2"
    t.string   "ship_address_3"
    t.string   "ship_city"
    t.string   "ship_state"
    t.string   "ship_postal_code"
    t.string   "ship_country"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "authentications", :force => true do |t|
    t.string   "provider"
    t.string   "provider_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "authentications", ["provider", "provider_id"], :name => "index_authentications_on_provider_and_provider_id", :unique => true

  create_table "contexts", :force => true do |t|
    t.integer  "account_type"
    t.integer  "account_id"
    t.string   "name"
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "controller_threads", :force => true do |t|
    t.string   "thread_type"
    t.boolean  "active"
    t.integer  "sleep_secs"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                 :default => "", :null => false
    t.string   "encrypted_password",     :limit => 128, :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                         :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "authentication_token"
    t.string   "token_authenticatable"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end

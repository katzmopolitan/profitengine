class CreateAmazonListingsCustomInfos < ActiveRecord::Migration
  def self.up
    create_table :amazon_listings_custom_infos do |t|
      t.integer :amazon_account_id
      t.string :listing_id
      t.string :sku
      t.boolean :reprice

      t.timestamps
    end
  end

  def self.down
    drop_table :amazon_listings_custom_infos
  end
end

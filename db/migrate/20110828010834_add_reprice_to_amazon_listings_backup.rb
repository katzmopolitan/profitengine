class AddRepriceToAmazonListingsBackup < ActiveRecord::Migration
  def self.up
    add_column :amazon_listings_backups, :reprice, :boolean
  end

  def self.down
    remove_column :amazon_listings_backups, :reprice
  end
end

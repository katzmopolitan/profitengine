class AddSleepSecsToAmazonAccount < ActiveRecord::Migration
  def self.up
    add_column :amazon_accounts, :sleep_secs, :integer
  end

  def self.down
    remove_column :amazon_accounts, :sleep_secs
  end
end

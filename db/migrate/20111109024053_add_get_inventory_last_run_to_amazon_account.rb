class AddGetInventoryLastRunToAmazonAccount < ActiveRecord::Migration
  def self.up
    add_column :amazon_accounts, :get_inventory_last_run, :datetime
  end

  def self.down
    remove_column :amazon_accounts, :get_inventory_last_run
  end
end

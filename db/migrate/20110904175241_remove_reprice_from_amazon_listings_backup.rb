class RemoveRepriceFromAmazonListingsBackup < ActiveRecord::Migration
  def self.up
    remove_column :amazon_listings_backups, :reprice
  end

  def self.down
    add_column :amazon_listings_backups, :reprice, :boolean
  end
end

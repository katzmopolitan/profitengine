class AddRepriceToAmazonListing < ActiveRecord::Migration
  def self.up
    add_column :amazon_listings, :reprice, :boolean
  end

  def self.down
    remove_column :amazon_listings, :reprice
  end
end

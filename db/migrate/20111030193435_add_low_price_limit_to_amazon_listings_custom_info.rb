class AddLowPriceLimitToAmazonListingsCustomInfo < ActiveRecord::Migration
  def self.up
    add_column :amazon_listings_custom_infos, :low_price_limit, :float
  end

  def self.down
    remove_column :amazon_listings_custom_infos, :low_price_limit
  end
end

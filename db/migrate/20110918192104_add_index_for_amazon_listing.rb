class AddIndexForAmazonListing < ActiveRecord::Migration
  def self.up
    add_index :amazon_listings, :amazon_account_id
    add_index :amazon_listings, :listing_id
    add_index :amazon_listings, :sku


    add_index :amazon_listings_custom_infos, :amazon_account_id
    add_index :amazon_listings_custom_infos, :listing_id
    add_index :amazon_listings_custom_infos, :sku
  end

  def self.down
    remove_index :amazon_listings, :amazon_account_id
    remove_index :amazon_listings, :listing_id
    remove_index :amazon_listings, :sku

    remove_index :amazon_listings_custom_infos, :amazon_account_id
    remove_index :amazon_listings_custom_infos, :listing_id
    remove_index :amazon_listings_custom_infos, :sku
  end
end

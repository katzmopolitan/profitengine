class CreateContexts < ActiveRecord::Migration
  def self.up
    create_table :contexts do |t|
      t.integer :account_type
      t.integer :account_id
      t.string :name
      t.string :value

      t.timestamps
    end
  end

  def self.down
    drop_table :contexts
  end
end

class CreateAmazonAccounts < ActiveRecord::Migration
  def self.up
    create_table :amazon_accounts do |t|
      t.integer :user_id
      t.string :merchant_id
      t.string :marketplace_id
      t.boolean :reprice

      t.timestamps
    end
  end

  def self.down
    drop_table :amazon_accounts
  end
end

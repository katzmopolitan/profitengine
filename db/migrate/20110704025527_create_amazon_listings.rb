class CreateAmazonListings < ActiveRecord::Migration
  def self.up
    create_table :amazon_listings do |t|
      t.integer :amazon_account_id
      t.string :item_name
      t.text :item_description
      t.string :listing_id
      t.string :sku
      t.float :price
      t.float :lowest_used_price
      t.float :lowest_new_price
      t.float :lowest_collectable_price
      t.float :lowest_refurbished_price
      t.integer :quantity
      t.datetime :open_date
      t.text :image_url
      t.string :item_is_marketplace
      t.integer :product_id_type
      t.float :zshop_shipping_fee
      t.text :item_note
      t.integer :item_condition
      t.integer :zshop_category1
      t.string :zshop_browse_path
      t.string :zshop_storefront_feature
      t.string :asin1
      t.string :asin2
      t.string :asin3
      t.string :will_ship_internationally
      t.string :expedited_shipping
      t.string :zshop_boldface
      t.string :product_id
      t.string :bid_for_featured_placement
      t.string :add_delete
      t.integer :pending_quantity
      t.string :fulfillment_channel

      t.timestamps
    end
  end

  def self.down
    drop_table :amazon_listings
  end
end

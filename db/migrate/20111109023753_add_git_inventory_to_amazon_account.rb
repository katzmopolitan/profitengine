class AddGitInventoryToAmazonAccount < ActiveRecord::Migration
  def self.up
    add_column :amazon_accounts, :get_inventory, :boolean
  end

  def self.down
    remove_column :amazon_accounts, :get_inventory
  end
end

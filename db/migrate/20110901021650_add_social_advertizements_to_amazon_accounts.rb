class AddSocialAdvertizementsToAmazonAccounts < ActiveRecord::Migration
  def self.up
    add_column :amazon_accounts, :social_ads, :boolean, :default=>true
  end

  def self.down
    remove_column :amazon_accounts, :social_ads
  end
end

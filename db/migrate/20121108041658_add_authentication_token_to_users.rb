class AddAuthenticationTokenToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :authentication_token, :string, :unique => true
    add_column :users, :token_authenticatable, :string
  end

  def self.down
    remove_column :users, :authentication_token
    remove_column :users, :token_authenticatable
  end
end

class RemoveRepriceFromAmazonListing < ActiveRecord::Migration
  def self.up
    remove_column :amazon_listings, :reprice
  end

  def self.down
    add_column :amazon_listings, :reprice, :boolean
  end
end

class CreateControllerThreads < ActiveRecord::Migration
  def self.up
    create_table :controller_threads do |t|
      t.string :thread_type
      t.boolean :active
      t.integer :sleep_secs
      t.timestamps
    end

    ControllerThread.create(:thread_type=>'CENTRAL', :active=>true, :sleep_secs=>60)
    ControllerThread.create(:thread_type=>'RE_PRICE', :active=>true, :sleep_secs=>85)

  end

  def self.down
    drop_table :controller_threads
  end
end

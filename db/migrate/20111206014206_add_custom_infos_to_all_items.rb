class AddCustomInfosToAllItems < ActiveRecord::Migration
  def self.up
    AmazonListing.all.each{|x|x.send(:create_amazon_listings_custom_info!)}
  end

  def self.down
  end
end

class CreateAmazonUnshippedItems < ActiveRecord::Migration
  def self.up
    create_table :amazon_unshipped_items do |t|
      t.integer :amazon_account_id
      t.string :order_id
      t.string :order_item_id
      t.string :status
      t.datetime :purchase_date
      t.datetime :payments_date
      t.datetime :reporting_date
      t.datetime :promise_date
      t.integer :days_past_promise
      t.string :buyer_email
      t.string :buyer_name
      t.string :buyer_phone_number
      t.string :sku
      t.string :product_name
      t.integer :quantity_purchased
      t.integer :quantity_shipped
      t.integer :quantity_to_ship
      t.string :ship_service_level
      t.string :recipient_name
      t.string :ship_address_1
      t.string :ship_address_2
      t.string :ship_address_3
      t.string :ship_city
      t.string :ship_state
      t.string :ship_postal_code
      t.string :ship_country

      t.timestamps
    end
  end

  def self.down
    drop_table :amazon_unshipped_items
  end
end

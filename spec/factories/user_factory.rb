Factory.sequence :email do |n|
  "user#{n}@example.com"
end

Factory.define :user do |u|
  u.email { Factory.next :email }
  u.password "secret123"
  u.password_confirmation {|u| u.password }
end

Factory.define :email_confirmed_user, :parent=>:user do |u|
  u.confirmed_at Time.now
end
Factory.sequence :sku do |n|
  "12#{n}".to_i
end

Factory.sequence :listing_id do |n|
  "452#{n}".to_i
end

Factory.define :amazon_listing do |a|
  a.sku { Factory.next :sku }
  a.listing_id { Factory.next :listing_id }

end

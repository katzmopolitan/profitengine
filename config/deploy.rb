# RVM bootstrap
#$:.unshift(File.expand_path("~/.rvm/lib"))
#require 'rvm/capistrano'
#set :rvm_ruby_string, '1.9.2'
#set :rvm_type, :user

# main details
set :application, "awsclown"
role :web, "profitengine.revion.net"
role :app, "profitengine.revion.net"
role :db, "profitengine.revion.net", :primary => true

# server details
default_run_options[:pty] = true
default_environment['PATH'] = "$PATH:/usr/local/bin/"
ssh_options[:forward_agent] = true
ssh_options[:forward_agent] = true
ssh_options[:port] = 2222
ssh_options[:keys] = [File.join(ENV["HOME"], ".ssh", "id_ilyakatz_gmail_rsa"),File.join(ENV["HOME"], ".ssh", "id_rsa")]
set :deploy_to, "/home/profitengine/data/awsclown"
set :deploy_via, :remote_cache
set :user, "profitengine"
set :use_sudo, false

# repo details
set :scm, :git
set :scm_username, "git"
set :repository, "git@heroku.com:awsclown.git"
set :branch, "master"
set :git_enable_submodules, 1

# runtime dependencies
depend :remote, :gem, "bundler", ">=1.0.0.rc.2"

before "deploy:update_code", :create_release_dir
before "deploy:update_code", :install_bundler

# tasks
task :create_release_dir do
  run "if [ ! -d /home/profitengine/data/awsclown/releases ]; then mkdir /home/profitengine/data/awsclown/releases; fi"
end

task :install_bundler do
  run "if [ -z  `which bundle` ]; then echo 'no bundler!' && gem install bundler; fi"
end

namespace :deploy do

  task :start, :roles => :app do
    run "touch #{current_path}/tmp/restart.txt"
  end

  task :stop, :roles => :app do
    # Do nothing.
  end

  desc "Restart Application"
  task :restart, :roles => :app do
    run "touch #{current_path}/tmp/restart.txt"
  end

  desc "Symlink shared resources on each release"
  task :symlink_shared, :roles => :app do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    run "ln -nfs #{shared_path}/config/amazon_account.yml #{release_path}/config/amazon_account.yml"
    run "ln -nfs #{shared_path}/config/sendgrid.yml #{release_path}/config/sendgrid.yml"
    run "rm #{release_path}/.rvmrc"
  end
end

after 'deploy:update_code', 'deploy:symlink_shared'

namespace :bundler do
  desc "Symlink bundled gems on each release"
  task :symlink_bundled_gems, :roles => :app do
    run "mkdir -p #{shared_path}/bundled_gems"
    run "mkdir #{release_path}/vendor/"
    run "ln -nfs #{shared_path}/bundled_gems #{release_path}/vendor/bundle"
  end

  desc "Install for production"
  task :install, :roles => :app do
    run "cd #{release_path} && RAILS_ENV=production bundle install --deployment --without test:development"
  end

end

namespace :db do
  desc "Migrate database changes"
  task :migrate, :roles => :app do
    run "cd #{release_path} && RAILS_ENV=production bundle exec rake db:migrate"
  end
end

after 'deploy:update_code', 'bundler:symlink_bundled_gems'
after 'deploy:update_code', 'bundler:install'
after 'deploy:update_code', 'db:migrate'

        require './config/boot'
        require 'hoptoad_notifier/capistrano'

JrubyRailsApp::Application.routes.draw do

  get "/" => 'home#index', :as=>"root"

  devise_for :users, :controllers => {:omniauth_callbacks => "users/omniauth_callbacks"}
  devise_for :users

  resources :amazon_accounts do
    post "/reprice/enable" => "reprice#enable", :as=>:reprice_enable
    post "/reprice/disable" => "reprice#disable", :as=>:reprice_disable
    get "/ads/disable" => "advertizements#disable", :as=>:ads_disable
    get "refresh" => "amazon_accounts#refresh", :as=>:refresh
  end

  resources :amazon_listings, :only=>[:index] do
    #seems to be a pain the butt when javascript is off
    get "/reprice/disable" => "reprice#amazon_listing_reprice_disable", :as=>:reprice_disable
    get "/reprice/enable" => "reprice#amazon_listing_reprice_enable", :as=>:reprice_enable
  end
  resources :home, :only=>[:index]
  resources :how_it_works, :only=>[:index]
  resources :about, :only=>[:index]
  resources :privacy, :only=>[:index]
  resources :contact
  resources :setup, :only=>[:index]

  match "mws" => "pages#mws", :as=>:mws_instructions


end


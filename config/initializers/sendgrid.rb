opts = YAML.load(ERB.new(File.read("#{Rails.root.to_s}/config/sendgrid.yml")).result)[Rails.env]
SENDGRID_USERNAME=opts["username"]
SENDGRID_PASSWORD=opts["password"]
SENDGRID_DOMAIN=opts["domain"]


ActionMailer::Base.smtp_settings = {
    :address => "smtp.sendgrid.net",
    :port => "25",
    :authentication => :plain,
    :user_name => SENDGRID_USERNAME,
    :password => SENDGRID_PASSWORD,
    :domain => SENDGRID_DOMAIN
}
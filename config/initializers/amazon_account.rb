opts          = YAML.load(ERB.new(File.read("#{Rails.root.to_s}/config/amazon_account.yml")).result)[Rails.env]
AWS_ACCESS_KEY=opts["access_key"]
AWS_SECRET_KEY=opts["secret_key"]
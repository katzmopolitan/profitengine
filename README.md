See [http://challengepost.com/software/profiteengine](http://challengepost.com/software/profiteengine)

ProfitEngine is a profit maximizer tool for sales on Amazon.com.

ProfitEngine works by continuously monitoring your Amazon account and updating your product prices, in this way giving you an instant advantage over all your competitors. It includes an easy to use interface that allows you to manage key aspects of your inventory in a very straightforward way.

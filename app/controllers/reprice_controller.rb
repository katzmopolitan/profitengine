class RepriceController < ApplicationController

  include ActionView::Helpers::NumberHelper

  def ssl_required?
    true
  end

  before_filter :authenticate_user!

  def enable
    #make sure that i'm the owner of the account
    aws = AmazonAccount.find_by_id_and_user_id(params[:amazon_account_id], current_user.id)
    aws.reprice = true
    if !aws.save
      Rails.logger.error(aws.errors)
      render :text=>"There was an error with your account. Please contact customer service", :status => :internal_server_error and return
    end
    @listings = current_user.amazon_listings.page(params[:page]).per(10)
  end

  def disable
    amazon_account = AmazonAccount.find_by_id_and_user_id(params[:amazon_account_id], current_user.id)
    amazon_account.reprice = false
    if !amazon_account.save
      Rails.logger.error(amazon_account.errors)
      render :text=>"There was an error with your account. Please contact customer service", :status => :internal_server_error and return
    end
    @listings = current_user.amazon_listings.page(params[:page]).per(10)
  end

  def amazon_listing_reprice_enable
    @amazon_listing=current_user.amazon_listings.find(params[:amazon_listing_id])
    if !@amazon_listing.amazon_account.reprice?
      render :text=>"Amazon Account is not configured for repricing", :status => :not_found and return
    end

    @amazon_listing.reprice_enable!
    if params[:low_price_limit].present?
      @amazon_listing.amazon_listings_custom_info.low_price_limit=params[:low_price_limit]
    else
      @amazon_listing.amazon_listings_custom_info.low_price_limit=number_with_precision(@amazon_listing.price/2, :precision=>2)
    end
    @amazon_listing.amazon_listings_custom_info.save!

  end

  def amazon_listing_reprice_disable
    @amazon_listing=current_user.amazon_listings.find(params[:amazon_listing_id])
    if !@amazon_listing.amazon_account.reprice?
      render :text=>"Amazon Account is not configured for repricing", :status => :not_found and return
    end

    if @amazon_listing.reprice_enabled?
      @amazon_listing.reprice_disable!
    end
  end

end
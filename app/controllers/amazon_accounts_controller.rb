class AmazonAccountsController < ApplicationController

  def ssl_required?
    true
  end

  before_filter :authenticate_user!

  def new
    if current_user.amazon_account
      redirect_to :action=>:index and return
    end
  end

  def create

    current_user.amazon_account = AmazonAccount.create(params[:amazon_account])

    @amazon_account = current_user.amazon_account
    if @amazon_account.save
      redirect_to amazon_listings_path and return
    end
    render :action=>:new and return
  end


  def show
    @amazon_account = AmazonAccount.find(params[:id], :conditions => {:user_id=>current_user.id})
    render :new
  end

  def update
    @amazon_account = AmazonAccount.find(params[:id], :conditions => {:user_id=>current_user.id})
    @amazon_account.attributes=params[:amazon_account]
    if @amazon_account.save
      flash[:notice]="Changes saved successfully"
      redirect_to :action=>:index and return
    end
    flash[:alert]="Could not update account"
    render :action=>:new and return
  end

  def refresh
    @amazon_account = AmazonAccount.find(params[:amazon_account_id], :conditions => {:user_id=>current_user.id})
    @amazon_account.get_inventory = true
    @amazon_account.save!
    redirect_to :controller=>:amazon_listings, :action=>:index
  end

end

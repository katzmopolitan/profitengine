class ApplicationController < ActionController::Base
  protect_from_forgery
  include MobilePath

  include ::SslRequirement

  layout :set_layout

  def set_layout
    if mobile_browser?
      "mobile"
    else
      "application"
    end
  end

  def after_sign_in_path_for(user)
    if user.amazon_listings.exists?
      amazon_listings_path
    else
      new_amazon_account_path
    end
  end

  def default_url_options(options={})
    {:format => params[:format]}
  end

end

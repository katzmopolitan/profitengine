class AmazonListingsController < ApplicationController

  def ssl_required?
    true
  end

  before_filter :authenticate_user!

  def index
    @amazon_account = current_user.amazon_account
    unless @amazon_account
      flash[:alert]="Please set up your amazon account first"
      redirect_to new_amazon_account_path
    end
    @per_page = per_page

    scope = current_user.amazon_listings.page(params[:page]).scoped
    unless @per_page == "all"
      scope = scope.per(@per_page||10)
    end

    if params[:search]
      scope = scope.where("item_name like '%#{params[:search][:query]}%' or sku like '%#{params[:search][:query]}%'")
    end

    @amazon_listings = scope
    
  end

  protected

  def per_page
    params[:listings].try(:[], :per_page) || params[:per_page]
  end

end

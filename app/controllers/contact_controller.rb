class ContactController < ApplicationController
  
  def index

  end

  def create
    ContactMailer.send_new_mail(params[:contact][:name],params[:contact][:email],params[:contact][:message]).deliver!
    redirect_to contact_index_path, :notice=>"Your message has been sent"
  end

end

class AdvertizementsController < ApplicationController

  def disable
    aws = AmazonAccount.find_by_id_and_user_id(params[:amazon_account_id], current_user.id)
    aws.social_ads = false
    if !aws.save
      Rails.logger.error(aws.errors)
      render :text=>"There was an error with your account. Please contact customer service", :status => :internal_server_error and return
    end
  end

end
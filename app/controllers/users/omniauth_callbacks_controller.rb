class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def ssl_required?
    true
  end

  def facebook
    @login = User.find_for_facebook_oauth(env["omniauth.auth"], current_user)

    if @login.persisted?
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Facebook"
      sign_in_and_redirect @login, :event => :authentication
    else
      session["devise.facebook_data"] = env["omniauth.auth"]
      redirect_to new_login_registration_url
    end
  end

  def google_apps
    # You need to implement the method below in your model
    @login = User.find_for_google_apps_oauth(env["omniauth.auth"], current_user)

    if @login.persisted?
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Gmail"
      sign_in_and_redirect @login, :event => :authentication
    else
      session["devise.google_apps_data"] = env["omniauth.auth"]
      redirect_to new_login_registration_url
    end
  end

#  def linked_in
#    # You need to implement the method below in your model
#    @login = User.find_for_linked_in_oauth(env["omniauth.auth"], current_user)
#
#    if @login.persisted?
#      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "LinkedIn"
#      sign_in_and_redirect @login, :event => :authentication
#    else
#      session["devise.google_apps_data"] = env["omniauth.auth"]
#      redirect_to new_login_registration_url
#    end
#  end

  def passthru
    render :file => "#{Rails.root}/public/404.html", :status => 404, :layout => false
  end

end



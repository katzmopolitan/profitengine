class ContactMailer < ActionMailer::Base

  def send_new_mail(name, email, message)
    from = "#{name} via PricingEngine <info@profit-engine.com>"
    @message = message
    @email=email
    mail(:to => SUPPORT_MAILING_LIST, :from=>from, :subject=>"New message from #{email}")
  end

end

class AmazonAccount < ActiveRecord::Base

  require "amazonmws"

  validates_presence_of :merchant_id, :marketplace_id
  validates_uniqueness_of :merchant_id
  has_many :amazon_listings
  has_many :amazon_unshipped_items
  belongs_to :user

  before_save :validate_marketplace_id, :validate_merchant_id
  before_save :validate_amazon_account_credentials if Rails.env != "development"

  attr_accessible :merchant_id, :marketplace_id, :user_id, :social_ads
  attr_accessible :reprice_last_run,:get_inventory,:get_inventory_last_run

  protected

  def validate_merchant_id
    if self.merchant_id =~ /\s/
      self.errors.add :merchant_id, "Merchant id cannot contain spaces"
      false
    end
  end

  def validate_marketplace_id
    if self.marketplace_id =~ /\s/
      self.errors.add :marketplace_id, "Marketplace id cannot contain spaces"
      false
    end
  end

  def validate_amazon_account_credentials

    if merchant_id_changed? or marketplace_id_changed? or reprice_changed?

      status = true

      mws = AmazonMWS::Base.new(
          "access_key"=>::AWS_ACCESS_KEY,
          "secret_access_key"=>::AWS_SECRET_KEY,
          "merchant_id"=>merchant_id,
          "marketplace_id"=>marketplace_id
      )


      response = mws.get_report_request_count

      if response.accessors.include?("code")
        if response.code == "AccessDenied"
          Rails.logger.info "Forbidden to access #{merchant_id}"
          self.errors.add :merchant_id, "Forbidden to access to #{merchant_id}"
          status = false
        else
          Rails.logger.info "Error: #{response.code} : #{response.message}"
          self.errors.add :base, "Unable to validate store"
          status = false
        end
      end

      status
    end
  end


end

class Listing < ActiveRecord::Base

  belongs_to :user
  belongs_to :aws_setup

  validates_presence_of :aws_setup_id
  validates_presence_of :user

  def reprice_enabled?
    aws_setup && aws_setup.reprice? && reprice?
  end
end

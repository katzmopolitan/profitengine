class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :encryptable, :lockable, :timeoutable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         #:confirmable,
         :omniauthable
  devise :token_authenticatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me

  has_one :amazon_account
  has_many :amazon_listings, :through => :amazon_account, :readonly=>false

  before_save :reset_authentication_token


  def self.find_for_facebook_oauth(access_token, signed_in_resource=nil)
    data = access_token['extra']['user_hash']
    if user = Authentication.find_by_provider_id_and_provider(data["id"], "facebook").try(:user)
      user
    elsif login = User.find_by_email(data["email"])
      authentication = Authentication.create!(:provider=>"facebook",
                                              :provider_id=>data["id"])
      authentication.user=login
      authentication.save!
      login
    else # Create a user with a stub password.
      authentication = Authentication.create!(:provider=>"facebook",
                                              :provider_id=>data["id"])
      user = User.new(
          :email => data["email"],
          :password => Devise.friendly_token[0, 20])
      user.save!
      authentication.user=user
      authentication.save!
      user
    end
  end

  def self.find_for_google_apps_oauth(access_token, signed_in_resource=nil)
    data = access_token['user_info']
    if user = User.find_by_email(data["email"])
      user
    else # Create a user with a stub password.
      user = User.new(
          :email => data["email"],
          :password => Devise.friendly_token[0, 20])
      user.save!
      user
    end
  end

#  def self.find_for_linked_in_oauth(access_token, signed_in_resource=nil)
#    data = access_token['extra']['user_hash']
#    if user = Authentication.find_by_provider_id_and_provider(access_token["uid"], "linkedin").try(:user)
#      user
#    else # Create a user with a stub password.
#      authentication = Authentication.create!(:provider=>"linkedin",
#                                              :provider_id=>access_token["uid"])
#      user = User.new(
#          :password => Devise.friendly_token[0, 20])
#      user.save!(:validate=>false)
#      authentication.user=user
#      authentication.save!
#      user
#    end
#  end

end

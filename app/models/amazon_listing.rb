class AmazonListing < ActiveRecord::Base
  belongs_to :amazon_account

  validates_presence_of :amazon_account

  has_one :amazon_listings_custom_info,
          :foreign_key => "listing_id",
          :primary_key => "listing_id",
          :conditions => ["amazon_account_id = amazon_listings_custom_infos.amazon_account_id"]

  after_create :create_amazon_listings_custom_info!

  def reprice_enabled?
    amazon_account && amazon_account.reprice? && reprice?
  end

  def item_condition_verbose
    case item_condition
      when 1
        "Used - Like New"
      when 2
        "Used - Very Good"
      when 3
        "Used - Good"
      when 4
        "Used - Acceptable"
      when 5
        "Collectible - Like New"
      when 6
        "Collectible - Very Good"
      when 7
        "Collectible - Good"
      when 8
        "Collectible - Acceptable"
      when 9
        "Not used (Amazon Internal)"
      when 10
        "Refurbished"
      when 11
        "New"
      else
        "?"
    end
  end

  def reprice_enable!

    if self.amazon_listings_custom_info.present?
      self.amazon_listings_custom_info.update_attribute(:reprice, true)
      if amazon_listings_custom_info.low_price_limit.nil?
        amazon_listings_custom_info.update_attribute(:low_price_limit, price/2)
      end
    else
      self.amazon_listings_custom_info = AmazonListingsCustomInfo.create(
          :amazon_account_id => amazon_account_id,
          :listing_id => listing_id,
          :sku => sku,
          :reprice => true,
          :low_price_limit => price/2)
      reload

    end
  end

  def reprice_disable!
    if self.amazon_listings_custom_info.present?

      amazon_listings_custom_info.update_attribute(:reprice, false)

    else

      AmazonListingsCustomInfo.create!(:amazon_account_id => amazon_account_id,
                                       :listing_id => listing_id,
                                       :sku => sku,
                                       :reprice => false)
    end
  end

  protected

  def reprice?
    amazon_listings_custom_info and amazon_listings_custom_info.reprice?
  end

  def create_amazon_listings_custom_info!
    unless amazon_listings_custom_info
      self.amazon_listings_custom_info = AmazonListingsCustomInfo.create(:amazon_account_id => self.amazon_account_id)
    end
  end

end

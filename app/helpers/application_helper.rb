module ApplicationHelper
  def check_if_current_url_starts_with(url_as_string)
    current_uri = request.env['PATH_INFO']
    return current_uri.starts_with? url_as_string
  end

  def check_if_current_url_same_as(url_as_string)
    current_uri = request.env['PATH_INFO']
    return current_uri == url_as_string
  end


  def get_active_url_css(active)
    if active then
      return "active"
    end
  end

  def facebook_like
    content_tag :iframe, nil,
                :src => "http://www.facebook.com/plugins/like.php?href=#{CGI::escape(TOP_URL)}&layout=standard&show_faces=true&width=450&action=like&font=arial&colorscheme=light&height=80",
                :scrolling => 'no',
                :frameborder => '0',
                :allowtransparency => true,
                :id => :facebook_like
  end


end
